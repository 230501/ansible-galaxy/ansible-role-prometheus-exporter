#!/bin/bash

export LANG=C

declare -a logs=( "/var/log/nginx/access.log", "/var/log/nginx/moypolk.ru.access.log" )

cat << EOF > /var/lib/node_exporter/textfile_collector/unique_ip_text_collector.prom
# HELP unique_ip_for_count_access_to_site Number of unique addresses in 1 hour.
# TYPE unique_ip_for_count_access_to_site untyped
EOF

for log in "${logs[@]}"
do
  echo unique_ip_count_for_access_to_site{log_file=\"$log\"} $(grep $(date --date="1 hour ago"  +%d/%b/%Y:%H) "$log"{,.1} | awk '{print $1}' | sort | uniq | wc -l) >> /var/lib/node_exporter/textfile_collector/unique_ip_text_collector.prom
done

chown node-exporter /var/lib/node_exporter/textfile_collector/unique_ip_text_collector.prom
